Utilities Library

Authors: T. P. Straatsma, Oak Ridge National Laboratory, Oak Ridge, TN

Downloading from the GitLab repository:

git clone --recursive git@gitlab.com:tpsutils/utils.git

The initial directory structure is as follows:

utils ---- src            (source directory with sub-directories)
    	|
		-- include        (with a few include files)
		|
		-- CMakeLists.txt (the cmake build file)
		|
		-- CTestConfig.cmake (cmake script to setup automated testing)
		|
		-- linux_compile

To build, do the following within the gronor directory:

1. mkdir build
2. cd build
3. cmake [flags] ../
4. make -j 10
5. make install

The compile script linux_compile can be edited and used to compile and install

This will expand the directory structure as follows:

gronor ---- src               (source directory with sub-directories)
  	     |
	 	-- include           (with a few include files)
   	     |
	 	-- CMakeLists.txt    (the cmake build file)
		|
		-- CTestConfig.cmake (cmake script to setup automated testing)
		|
		-- build --- bin     (directory with the utils binaries, currently unused)
		          |
				  -- lib     (directory with the utils libraries)
		  			|
				  -- include (directory with the utils module files)
		  			|
		 		 -- CMakeFiles  (cmake files created during build)


The following rules need to be followed for the src subdirectories:

1. programs in a single source file in a subdirectory with the same name
2. library files in a subdirectory will be in a single library
