      module tpsfun

      implicit none

      contains
      
      real (kind=8) function erfc(x)
      real (kind=8), intent(in) :: x
      erfc=1.0d0/((1.0d0+x*(7.05230784d-02+x*(4.22820123d-02+
     +     x*(9.2705272d-03+x*(1.520143d-04+x*(2.765672d-04+
     +     x*4.30638d-05))))))**16)
      end function erfc

      real (kind=8) function derfc(x)
      real (kind=8), intent(in) :: x
      derfc=(-1.6d+01)*(7.05230784d-02+x*(8.45640246d-02+
     +     x*(2.78115816d-02+x*(6.080574d-04+x*(1.382836d-03+
     +     x*2.583828d-04)))))/
     +     ((1.0d0+x*(7.05230784d-02+x*(4.22820123d-02+
     +     x*(9.2705272d-03+x*(1.520143d-04+x*(2.765672d-04+
     +     x*4.30638d-05))))))**17)
      end function derfc
      
      end module tpsfun
